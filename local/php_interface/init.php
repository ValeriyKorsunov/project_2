<?php
/**
 * Created by PhpStorm.
 * User: korsunov
 * Date: 24.06.2017
 * Time: 14:00
 */
/*
echo '<pre>';
    var_dump($_POST);
echo '</pre>';
*/
use \Bitrix\Main;
$eventManager = Main\EventManager::getInstance();
//Обработчик в файле /bitrix/php_interface/init.php
$eventManager->AddEventHandler("main", "OnBeforeEventAdd", array("MyClass", "OnBeforeEventAddHandler"));

class MyClass
{
    function OnBeforeEventAddHandler(&$event, &$lid, &$arFields)
    {
        $arFields["USER_PHONE"] =  htmlspecialchars($_POST["USER_PHONE"]);//"Новый макрос для почтового шаблона";
        //$arFields["VS_BIRTHDAY"] = "Изменение существующего макроса";
        //$lid = 's2'; //Изменяем привязку к сайту
    }
}
?>