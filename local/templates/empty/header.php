<?
	if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)die();
    IncludeTemplateLangFile(__FILE__);
    $isMainPage = $APPLICATION->GetCurPage(true) == SITE_DIR."index.php";
    use \Bitrix\Main\Page\Asset;
    
?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <?$APPLICATION->ShowHead();?>
    <title><?$APPLICATION->ShowTitle();?></title>
    <meta name="viewport" content="width=1100">
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <?
        Asset::getInstance()->addCss(SITE_TEMPLATE_PATH.'/css/normalize.css');
        Asset::getInstance()->addCss(SITE_TEMPLATE_PATH.'/css/main.css');
        Asset::getInstance()->addCss(SITE_TEMPLATE_PATH.'/css/jquery.fancybox.css');
        Asset::getInstance()->addCss(SITE_TEMPLATE_PATH.'/css/litetabs.css');
    ?>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,700,300&subset=latin,cyrillic' rel='stylesheet' type='text/css'>

    <script>window.jQuery || document.write('<script src="<?=SITE_TEMPLATE_PATH?>/js/vendor/jquery-1.10.2.min.js"><\/script>')</script>
    <?
        Asset::getInstance()->addJs('//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js');
        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/jquery.jcarousel.min.js");
        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/jquery.fancybox.pack.js");
        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/litetabs.jquery.js");
        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/main.js");
    ?>
</head>

<body>
    <?$APPLICATION->ShowPanel()?>
    <!--[if lt IE 7]>
        <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <header>
        <a href="/" class="logo"></a>
        <div class="contacts">
            <div class="phone">
                <?$APPLICATION->IncludeFile(SITE_DIR."/include/telephone.php",
                                            array(),
                                            array(MODE => "html")
                                            );
                ?>
            </div>
            <div class="text">
                <?$APPLICATION->IncludeFile(SITE_DIR."/include/mail_header.php",
                    array(),
                    array(MODE => "html")
                );
                ?>
            </div>
        </div>
    </header>

    <?$APPLICATION->IncludeComponent("bitrix:menu", "menu_multilevel", Array(
        "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
        "CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
        "DELAY" => "N",	// Откладывать выполнение шаблона меню
        "MAX_LEVEL" => "2",	// Уровень вложенности меню
        "MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
        "MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
        "MENU_CACHE_TYPE" => "N",	// Тип кеширования
        "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
        "ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
        "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
        "COMPONENT_TEMPLATE" => "horizontal_multilevel",
        "MENU_THEME" => "site"
    ),
        false
    );?>

    <?if($isMainPage):?>
        <?$APPLICATION->IncludeComponent("bitrix:news.list", "index.list", Array(
            "ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
            "ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
            "AJAX_MODE" => "N",	// Включить режим AJAX
            "AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
            "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
            "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
            "AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
            "CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
            "CACHE_GROUPS" => "Y",	// Учитывать права доступа
            "CACHE_TIME" => "36000000",	// Время кеширования (сек.)
            "CACHE_TYPE" => "A",	// Тип кеширования
            "CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
            "DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
            "DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
            "DISPLAY_DATE" => "N",	// Выводить дату элемента
            "DISPLAY_NAME" => "N",	// Выводить название элемента
            "DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
            "DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
            "DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
            "FIELD_CODE" => array(	// Поля
                0 => "PREVIEW_PICTURE",
                1 => "",
            ),
            "FILTER_NAME" => "",	// Фильтр
            "HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
            "IBLOCK_ID" => $_REQUEST["ID"],	// Код информационного блока
            "IBLOCK_TYPE" => "-",	// Тип информационного блока (используется только для проверки)
            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
            "INCLUDE_SUBSECTIONS" => "N",	// Показывать элементы подразделов раздела
            "MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
            "NEWS_COUNT" => "20",	// Количество новостей на странице
            "PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
            "PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
            "PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
            "PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
            "PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
            "PAGER_TITLE" => "Новости",	// Название категорий
            "PARENT_SECTION" => "",	// ID раздела
            "PARENT_SECTION_CODE" => "",	// Код раздела
            "PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
            "PROPERTY_CODE" => array(	// Свойства
                0 => "",
                1 => "url",
                2 => "",
            ),
            "SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
            "SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
            "SET_META_DESCRIPTION" => "N",	// Устанавливать описание страницы
            "SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
            "SET_STATUS_404" => "N",	// Устанавливать статус 404
            "SET_TITLE" => "N",	// Устанавливать заголовок страницы
            "SHOW_404" => "N",	// Показ специальной страницы
            "SORT_BY1" => "TIMESTAMP_X",	// Поле для первой сортировки новостей
            "SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
            "SORT_ORDER1" => "DESC",	// Направление для первой сортировки новостей
            "SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
        ),
            false
        );?>
    <?else:?>
        <?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "breadcrumb", Array(
            "PATH" => "",	// Путь, для которого будет построена навигационная цепочка (по умолчанию, текущий путь)
            "SITE_ID" => "s1",	// Cайт (устанавливается в случае многосайтовой версии, когда DOCUMENT_ROOT у сайтов разный)
            "START_FROM" => "0",	// Номер пункта, начиная с которого будет построена навигационная цепочка
        ),
            false
        );?>
    <?endif;?>

    <main>
        <?if($isMainPage):?>
        <ul class="b_section_list clearfix">
            <li><a href=""><span class="pic" style="background-image: url(/img/s01.jpg);"></span><span class="name">Стальные двери</span></a></li>
            <li><a href=""><span class="pic" style="background-image: url(/img/s02.jpg);"></span><span class="name">Решетки</span></a></li>
            <li><a href=""><span class="pic" style="background-image: url(/img/s03.jpg);"></span><span class="name">Кованые изделия</span></a></li>
            <li><a href=""><span class="pic" style="background-image: url(/img/s04.jpg);"></span><span class="name">Гаражные ворота</span></a></li>
        </ul>
        <?else:?>
            <div class="lined_h1">
                <h1><?$APPLICATION->ShowTitle();?></h1>
            </div>
        <?endif;?>
