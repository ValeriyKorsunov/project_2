<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?
//  echo '<pre>'; var_dump($arResult['ITEMS']); echo '</pre>';

?>
    <?=$arResult['DESCRIPTION']?>
    <ul class="clearfix b_gallery_list">
        <?foreach ($arResult['ITEMS'] as $key => $arItem):?>       
        <li>
            <a rel="gallery" 
               href="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" 
               style="background-image: url(<?=$arItem['PREVIEW_PICTURE']['SRC']?>);"
               class="fancybox">
            </a>
            <a href="<?=$arItem['DETAIL_PAGE_URL']?>">
                <span class="name"><?=$arItem['NAME']?></span>
            </a>
        </li>
        <? endforeach;?>
    </ul>