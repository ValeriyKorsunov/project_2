<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
?> 
 <div id="feedback">
    <h2>Вызвать замерщика</h2>

    <form action="<?=POST_FORM_ACTION_URI?>" method="POST">
        <p>
        <?if(!empty($arResult["ERROR_MESSAGE"]))
        {
            foreach($arResult["ERROR_MESSAGE"] as $v)
                ShowError($v);
        }
        if(strlen($arResult["OK_MESSAGE"]) > 0)
        {
            echo $arResult["OK_MESSAGE"];
        }
        ?>
        </p>
        <?=bitrix_sessid_post()?>
        <ul>
            <li>
                <input type="text" name="user_name" class="req" placeholder="Ваше имя" value="<?=$arResult["AUTHOR_NAME"]?>">
            </li>
            <li>
                <input name="USER_PHONE"
                       type="text"
                       class="req"
                       value="<?=htmlspecialchars($_POST["USER_PHONE"])?>"
                       placeholder="Телефон" >
            </li>
            <li>
                <textarea name="" placeholder="Комментарий" cols="30" rows="10"></textarea>
            </li>
        </ul>
        <p><span class="red">*</span> — обязательные поля</p>
        <input class="btn" type="submit" name="submit" value="<?=GetMessage("MFT_SUBMIT")?>">
    </form>
</div>