<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)die();

$isContact = $APPLICATION->GetCurPage(FALSE) == SITE_DIR."contacts/";
?>
        </main>
        
        <div class="footer_wrapper">
        	<footer>
        	    <div class="copyright_company">
                    <?$APPLICATION->IncludeFile(SITE_DIR."/include/footer_text.php",
                                                array(),
                                                array(MODE => "html")
                                                );
                    ?>
        	    </div>
        	    <a href="#feedback" class="footer_btn fancybox"><span>Вызов замерщика</span></a>
        	    <div class="copyright_pixel">
        	        <a href="http://pixelplus.ru" target="_blank" class="logo"></a>
        	        <a href="http://pixelplus.ru" target="_blank">Создание сайта</a> – 
        	        <br>
        	        компания «<a href="http://pixelplus.ru" target="_blank">Пиксель Плюс</a>»
        	    </div>
        	</footer>
        </div>
        <div style='display:none;'>
            <?$APPLICATION->IncludeComponent(
                "bitrix:main.feedback",
                "feedback",
                array(
                    "CUSTOM_SORT_DATA" => "",
                    "EMAIL_TO" => "kors.vv@ya.ru",
                    "EVENT_MESSAGE_ID" => array('8'),
                    "OK_TEXT" => "Спасибо, ваше сообщение принято.",
                    "REQUIRED_FIELDS" => array("NAME",'USER_PHONE'),
                    "USE_CAPTCHA" => "N",
                    "COMPONENT_TEMPLATE" => "feedback"
                ),
                false
            );?>
        </div>
    </body>
</html>