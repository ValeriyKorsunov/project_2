<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Контакты");
?>

<div class="clearfix">
            	<div class="contacts_info">
            		<ul>
            			<li>
            				<div class="icon icon_01"></div>
            				<div class="text">
            					<b>Тел:</b> 8 (491) 246-00-64  
            					<br>
								<b>E-mail:</b> info@stan-doors.ru
            				</div>
            			</li>
            			<li>
            				<div class="icon icon_02"></div>
            				<div class="text">
            					<b>Адрес:</b> Клинская область,  
            					<br>
								д. Давыдково, стр. 18
            				</div>
            			</li>
            			<li>
            				<div class="icon icon_03"></div>
            				<div class="text">
            					<b>Часы работы:</b>   
            					<br>
								Пн-пт: 10:00-18:00  
								<br>
								Вых.: 10:00-14:00
            				</div>
            			</li>
            		</ul>
            		<a href="#feedback" class="btn fancybox">Вызов замерщика</a>
            	</div>
            	<div class="contacts_map">
            		<img src="/img/contacts_map.png" alt="">
            	</div>
            </div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>